package net.cubekrowd.eventstorageapi.api;

import java.util.*;
import lombok.*;

/**
 * Data storage object representing an Event, something which has happened. This different from Bukkit/Bungee EventListener events. EventEntry object should be created with the name of the plugin (with correct capitalisation), the name of the event and optionally timestamp and data. EventEntry objects are state-less and does not represent the actual state in the storage. EventEntry objects which are read from the database returns with an unmodifiable map.
 */
public final class EventEntry implements Comparable {

    /**
     * Returns the name of the plugin who owns the event.
     * Plugin names are always alphanumeric all-lowercase.
     *
     * @return The name of the plugin.
     */
    @Getter
    private final String plugin;

    /**
     * Returns what kind of event this is, i.e what data it may contain.
     *
     * @return The type of event.
     */
    @Getter
    private final String type;

    /**
     * Returns the time in milliseconds when this event took place.
     * This does not neccesarily equal the actual time it took place
     * or when it was inserted. This should never be a negative number.
     *
     * @return The time of the event.
     */
    @Getter
    private final long time;

    /**
     * Returns the data this event contains.
     *
     * @return This events data.
     */
    @Getter
    private final Map<String, String> data;

    /**
     * Creates a new Event with the specified plugin name and event type.
     *
     * @param plugin The name of the plugin.
     * @param type The event type. Correct capitalisation required.
     * @throws NullPointerException If any of the arguments is null.
     */
    public EventEntry(String plugin, String type) {
        this(plugin, type, System.currentTimeMillis());
    }

    /**
     * Creates a new Event with the specified plugin name, event type and time.
     *
     * @param plugin The name of the plugin.
     * @param type The event type. Correct capitalisation required.
     * @param time Time in milliseconds since epoch when this event was created or took place.
     * @throws NullPointerException If any of the arguments is null.
     */
    public EventEntry(String plugin, String type, long time) {
        this(plugin, type, time, new HashMap<>());
    }

    /**
     * Creates a new Event with the specified plugin name, event type and data.
     *
     * @param plugin The name of the plugin.
     * @param type The event type. Correct capitalisation required.
     * @param data The map containing this events data.
     * @throws NullPointerException If any of the arguments is null.
     */
    public EventEntry(String plugin, String type, Map<String, String> data) {
        this(plugin, type, System.currentTimeMillis(), data);
    }

    /**
     * Creates a new Event with the specified plugin name, event type, time and data.
     *
     * @param plugin The name of the plugin.
     * @param type The event type. Correct capitalisation required.
     * @param time Time in milliseconds since epoch when this event was created or took place.
     * @param data The map containing this events data.
     * @throws NullPointerException If any of the arguments is null.
     * @throws IllegalArgumentException If plugin or type is empty, or if time is negative.
     */
    public EventEntry(@NonNull String plugin, @NonNull String type, long time, @NonNull Map<String, String> data) {
        plugin = plugin.toLowerCase().replaceAll("[^a-z0-9_]", "");
        type = type.toLowerCase().replaceAll("[^a-z0-9_]", "");
        if(plugin.isEmpty()) {
            throw new IllegalArgumentException("Plugin name cannot be empty");
        }
        if(type.isEmpty()) {
            throw new IllegalArgumentException("Event type cannot be empty");
        }
        if(time < 0) {
            throw new IllegalArgumentException("Time cannot be negative");
        }
        this.plugin = plugin;
        this.type = type;
        this.time = time;
        this.data = data;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        if(!(o instanceof EventEntry)) {
            throw new ClassCastException("Object is not istanceof EventEntry, cannot compare!");
        }
        return Long.compare(getTime(), ((EventEntry) o).getTime());
    }

}
