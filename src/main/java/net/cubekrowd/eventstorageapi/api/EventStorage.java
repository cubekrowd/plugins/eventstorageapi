package net.cubekrowd.eventstorageapi.api;

import java.util.*;
import java.util.stream.*;

/**
 * Abstract database adapter representing the backend storage.
 * Do not save this class to a variable, rather always use the public EventStorageAPI class.
 * <br><br>
 *
 * <b>NOTE:</b> The event record is INSERT-READ-ONLY and it is not possible to remove stored events.
 */
public abstract class EventStorage {

    /**
     * <b>Internal method</b> for backend to manage the storage.
     * Depending on the implementation this method may throw an
     * exception if the storage is already opened.
     */
    public abstract void open();

    /**
     * <b>Internal method</b> for backend to manage the storage.
     * Depending on the implementation this method may throw an
     * exception if the storage is already closed.
     */
    public abstract void close();

    /**
     * Returns a List of all plugins names which have stored data.
     *
     * <b>Note:</b> The plugin names returned are all-lowercase.
     */
    public abstract List<String> getPlugins();

    /**
     * Returns a list of all events known in use by a plugin, empty list if plugin is not known.
     */
    public abstract List<String> getEventTypes(String plugin);

    /**
     * Adds a new EventEntry to the event record.
     *
     * @param eventEntry The event entry to insert into the storage.
     * @throws NullPointerException If the argument is null.
     */
    public abstract void addEntry(EventEntry eventEntry);

    /**
     * Returns a List of EventEntry entires matching the specified search criteria.
     *
     * @param plugin The name of the plugin.
     * @param type The event type. Correct capitalisation required.
     * @throws NullPointerException If any of the arguments is null.
     * @throws IllegalArgumentException If plugin or type is empty.
     */
    public final List<EventEntry> getEntryList(String plugin, String type) {
        return getEntryList(plugin, type, -1, -1);
    }

    /**
     * Returns a List of EventEntry entires matching the specified search criteria within a timeframe.
     *
     * @param plugin The name of the plugin.
     * @param type The event type. Correct capitalisation required.
     * @param startTime Start time (Inclusive) in milliseconds since epoch.
     * @param endTime End time (Excluding) in milliseconds since epoch.
     * @throws NullPointerException If any of the arguments is null.
     * @throws IllegalArgumentException If plugin or type is empty.
     */
    public final List<EventEntry> getEntryList(String plugin, String type, long startTime, long endTime) {
        return getEntryList(plugin, Arrays.asList(type), startTime, endTime);
    }

    /**
     * Returns a List of EventEntry entires matching the specified search criteria.
     *
     * @param plugin The name of the plugin.
     * @param types List of event types. Correct capitalisation required.
     * @throws NullPointerException If any of the arguments is null.
     * @throws IllegalArgumentException If plugin or type is empty.
     */
    public final List<EventEntry> getEntryList(String plugin, List<String> types) {
        return getEntryList(plugin, types, -1, -1);
    }

    /**
     * Returns a List of EventEntry entires matching the specified search criteria within a timeframe.
     *
     * @param plugin The name of the plugin.
     * @param types List of event types. Correct capitalisation required.
     * @param startTime Start time (Inclusive) in milliseconds since epoch.
     * @param endTime End time (Excluding) in milliseconds since epoch.
     * @throws NullPointerException If any of the arguments is null.
     * @throws IllegalArgumentException If plugin or type is empty.
     */
    public final List<EventEntry> getEntryList(String plugin, List<String> types, long startTime, long endTime) {
        return getEntryList(plugin, types, startTime, endTime, Collections.emptyMap());
    }

    /**
     * Returns a List of EventEntry entires matching the specified search criteria within a timeframe.
     *
     * @param plugin The name of the plugin.
     * @param types List of event types. Correct capitalisation required.
     * @param filter List of data fields with required equal value.
     * @throws NullPointerException If any of the arguments is null.
     * @throws IllegalArgumentException If plugin or type is empty.
     */
    public final List<EventEntry> getEntryList(String plugin, List<String> types, Map<String, String> filter) {
        return getEntryList(plugin, types, -1, -1, filter);
    }

    /**
     * Returns a List of EventEntry entires matching the specified search criteria within a timeframe.
     *
     * @param plugin The name of the plugin.
     * @param types List of event types. Correct capitalisation required.
     * @param startTime Start time (Inclusive) in milliseconds since epoch.
     * @param endTime End time (Excluding) in milliseconds since epoch.
     * @param filter List of data fields with required equal value.
     * @throws NullPointerException If any of the arguments is null.
     * @throws IllegalArgumentException If plugin or type is empty.
     */
    public abstract List<EventEntry> getEntryList(String plugin, List<String> types, long startTime, long endTime, Map<String, String> filter);

    /**
     * Returns a Stream of EventEntry entires matching the specified search criteria.
     *
     * @param plugin The name of the plugin.
     * @param type The event type. Correct capitalisation required.
     * @throws NullPointerException If any of the arguments is null.
     * @throws IllegalArgumentException If plugin or type is empty.
     */
    public final Stream<EventEntry> getEntryStream(String plugin, String type) {
        return getEntryStream(plugin, type, -1, -1);
    }

    /**
     * Returns a Stream of EventEntry entires matching the specified search criteria within a timeframe.
     *
     * @param plugin The name of the plugin.
     * @param type The event type. Correct capitalisation required.
     * @param startTime Start time (Inclusive) in milliseconds since epoch.
     * @param endTime End time (Excluding) in milliseconds since epoch.
     * @throws NullPointerException If any of the arguments is null.
     * @throws IllegalArgumentException If plugin or type is empty.
     */
    public final Stream<EventEntry> getEntryStream(String plugin, String type, long startTime, long endTime) {
        return getEntryStream(plugin, Arrays.asList(type), startTime, endTime);
    }

    /**
     * Returns a Stream of EventEntry entires matching the specified search criteria.
     *
     * @param plugin The name of the plugin.
     * @param types List event types. Correct capitalisation required.
     * @throws NullPointerException If any of the arguments is null.
     * @throws IllegalArgumentException If plugin or type is empty.
     */
    public final Stream<EventEntry> getEntryStream(String plugin, List<String> types) {
        return getEntryStream(plugin, types, -1, -1);
    }

    /**
     * Returns a Stream of EventEntry entires matching the specified search criteria.
     *
     * @param plugin The name of the plugin. Correct capitalisation required.
     * @param types List of event types. Correct capitalisation required.
     * @param startTime Start time (Inclusive) in milliseconds since epoch.
     * @param endTime End time (Excluding) in milliseconds since epoch.
     * @throws NullPointerException If any of the arguments is null.
     * @throws IllegalArgumentException If plugin or type is empty.
     */
    public final Stream<EventEntry> getEntryStream(String plugin, List<String> types, long startTime, long endTime) {
        return getEntryStream(plugin, types, startTime, endTime, Collections.emptyMap());
    }

    /**
     * Returns a Stream of EventEntry entires matching the specified search criteria.
     *
     * @param plugin The name of the plugin. Correct capitalisation required.
     * @param types List of event types. Correct capitalisation required.
     * @param filter List of data fields with required equal value.
     * @throws NullPointerException If any of the arguments is null.
     * @throws IllegalArgumentException If plugin or type is empty.
     */
    public final Stream<EventEntry> getEntryStream(String plugin, List<String> types, Map<String, String> filter) {
        return getEntryStream(plugin, types, -1, -1, filter);
    }

    /**
     * Returns a Stream of EventEntry entires matching the specified search criteria.
     *
     * @param plugin The name of the plugin. Correct capitalisation required.
     * @param types List of event types. Correct capitalisation required.
     * @param startTime Start time (Inclusive) in milliseconds since epoch.
     * @param endTime End time (Excluding) in milliseconds since epoch.
     * @param filter List of data fields with required equal value.
     * @throws NullPointerException If any of the arguments is null.
     * @throws IllegalArgumentException If plugin or type is empty.
     */
    public abstract Stream<EventEntry> getEntryStream(String plugin, List<String> types, long startTime, long endTime, Map<String, String> filter);

}
