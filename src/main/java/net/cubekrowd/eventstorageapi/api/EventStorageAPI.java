package net.cubekrowd.eventstorageapi.api;

import java.util.*;
import lombok.*;

/**
 * Main point-of-entry to the EventStorageAPI.
 */
// Prevent instansiation
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class EventStorageAPI {

    /**
     * Returns this APIs EventWorkbench instance.
     *
     * @returns The workbench instance.
     */
    @Getter private static final EventWorkbench workbench = new EventWorkbench();

    /**
     * Returns this APIs EventStorage instance.
     *
     * @returns The storage instance.
     */
    @Getter private static EventStorage storage = null;

    /**
     * <b>Internal method</b> for backend to register the storage.
     * Do not call as part of the public API. This function WILL throw an exception
     * if called more than once. The arguments may be null.
     *
     * @param storage The backend eventstorage adapter.
     * @throws UnsupportedOperationException If this function is called more than once.
     * @throws NullPointerException If any of the arguments is null.
     */
    public static void registerInit(@NonNull EventStorage storage) {
        if(EventStorageAPI.storage != null) {
            throw new UnsupportedOperationException("Cannot redefine singleton storage");
        }
        EventStorageAPI.storage = storage;
    }

}
