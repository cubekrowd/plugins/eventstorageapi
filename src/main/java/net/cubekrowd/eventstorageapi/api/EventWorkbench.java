package net.cubekrowd.eventstorageapi.api;

import java.util.*;
import lombok.*;

/**
 * Utiliy in-memory EventEntry storage for coding simplicity.
 * This class exists due to the inability of creating variables and storing
 * EventEntry objects if ESAPI is only an optional dependency. If attempted to
 * store EventEntry objects during runtime without ESAPI then java will throw
 * ClassNotFoundException.
 */
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public final class EventWorkbench {

    private final Map<String, Map<String, EventEntry>> inventory = new HashMap<>();

    /**
     * Creates a new EventEntry in the crafting workbench.
     *
     * @param plugin The name of the plugin.
     * @param key Unique key identifying this specific evententry.
     * @param type The event type. Correct capitalisation required.
     * @throws NullPointerException If any of the arguments is null.
     * @throws IllegalArgumentException If plugin, key or type is empty, or if an event with this key already exists.
     * @return The newly crafted EventEntry.
     */
    public EventEntry craftEntry(@NonNull String plugin, @NonNull String key, @NonNull String type) {
        plugin = plugin.toLowerCase().replaceAll("[^a-z0-9]", "");
        if(plugin.isEmpty()) {
            throw new IllegalArgumentException("Plugin name cannot be empty");
        }
        if(key.isEmpty()) {
            throw new IllegalArgumentException("Workbench key cannot be empty");
        }
        if(type.isEmpty()) {
            throw new IllegalArgumentException("Event type cannot be empty");
        }
        if(!inventory.keySet().contains(plugin)) {
            inventory.put(plugin, new HashMap<>());
        }
        if(inventory.get(plugin).containsKey(key)) {
            throw new IllegalArgumentException("Duplicate key collision! An event with this key already exists");
        }

        EventEntry entry = new EventEntry(plugin, type);
        inventory.get(plugin).put(key, entry);
        return entry;
    }

    /**
     * Returns an EventEntry from the workbench, or null if non-existent.
     *
     * @param plugin The name of the plugin.
     * @param key Unique key identifying this specific evententry.
     * @throws NullPointerException If any of the arguments is null.
     * @return The event, or null if not found.
     */
    public EventEntry getEntry(@NonNull String plugin, @NonNull String key) {
        plugin = plugin.toLowerCase().replaceAll("[^a-z0-9]", "");
        if(!inventory.keySet().contains(plugin)) {
            return null;
        }

        if(inventory.get(plugin).keySet().contains(key)) {
            return inventory.get(plugin).get(key);
        } else {
            return null;
        }
    }

    /**
     * Returns the list of keys for a plugin, or null if non-existent.
     *
     * @param plugin The name of the plugin. correct capitalisation required.
     * @throws NullPointerException If the argument is null.
     * @return List of all keys for the plugin, empty list if plugin not found.
     */
    public List<String> getKeys(@NonNull String plugin) {
        plugin = plugin.toLowerCase().replaceAll("[^a-z0-9]", "");
        if(inventory.get(plugin) == null) {
            return new ArrayList<>();
        }
        return new ArrayList<>(inventory.get(plugin).keySet());
    }

    /**
    * @See containsEntry
    */
    @Deprecated
    public boolean containsEvent(@NonNull String plugin, @NonNull String key) {
        return containsEntry(plugin, key);
    }
    /**
     * Returns a boolean state if this workbench contains an evnet with a specific key.
     *
     * @param plugin The name of the plugin. correct capitalisation required.
     * @param key Unique key identifying this specific evententry.
     * @throws NullPointerException If any of the arguments is null.
     * @return If this workspace contains an event with the specified key
     */
    public boolean containsEntry(@NonNull String plugin, @NonNull String key) {
        plugin = plugin.toLowerCase().replaceAll("[^a-z0-9]", "");
        if(!inventory.keySet().contains(plugin)) {
            return false;
        }

        return inventory.get(plugin).keySet().contains(key);
    }

    /**
    * @see removeEntry
    */
    @Deprecated
    public void removeEvent(@NonNull String plugin, @NonNull String key) {
        removeEntry(plugin, key);
    }
    /**
     * Deletes the EventEntry with the specified key. Does nothing if non-existent.
     *
     * @param plugin the name of the plugin. correct capitalisation required.
     * @param key unique key identifying this specific evententry.
     * @throws NullPointerException If any of the arguments is null.
     */
    public void removeEntry(@NonNull String plugin, @NonNull String key) {
        plugin = plugin.toLowerCase().replaceAll("[^a-z0-9]", "");
        if(!inventory.keySet().contains(plugin)) {
            return;
        }

        if(containsEvent(plugin, key)) {
            inventory.get(plugin).remove(key);
        }
    }

    /**
     * Deletes all EventEntry objects created by the specified plugin. Does nothing if none exist.
     *
     * @param plugin the name of the plugin.
     * @throws NullPointerException If the argument is null.
     */
    public void clearCrafting(@NonNull String plugin) {
        plugin = plugin.toLowerCase().replaceAll("[^a-z0-9]", "");
        if(inventory.keySet().contains(plugin)) {
            inventory.remove(plugin);
        }
    }

}
