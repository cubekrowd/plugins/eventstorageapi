package net.cubekrowd.eventstorageapi.api.event;

/**
 * Abstract interface representing any ESAPI Spigot/Bungee event.
 */
public enum ESAPIEvent {
    STORAGE_ENTRY_ADDED;
    private ESAPIEvent() {

    }
}
