package net.cubekrowd.eventstorageapi.api.event;

import net.cubekrowd.eventstorageapi.api.*;

/**
 * Custom Spigot/Bungee event representing the action after adding an entry to the storage.
 */
public interface StorageEntryAddedEvent {

    /**
     * Returns the EventEntry which was inserted in the storage.
     *
     * @return The EventEntry which triggered this event.
     */
    public EventEntry getEntry();

}
