package net.cubekrowd.eventstorageapi.bungeecord;

import java.util.*;
import java.util.function.*;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.*;
import net.md_5.bungee.api.plugin.Command;
import net.cubekrowd.eventstorageapi.common.command.*;

public class BungeeCommand extends Command implements ESAPICommand {

    private final EventStoragePluginBungee plugin;

    public BungeeCommand(EventStoragePluginBungee plugin) {
        super("esapi", null, new String[0]);
        this.plugin = plugin;
    }

    public void execute(CommandSender sender, String[] args) {
        executeCommand(plugin, new Messagable() {
            @Override
            public void sendMessage(BaseComponent component) {
                sender.sendMessage(component);
            }

            @Override
            public boolean hasPermission(String permission) {
                return sender.hasPermission(permission);
            }
        }, args);
    }

}
