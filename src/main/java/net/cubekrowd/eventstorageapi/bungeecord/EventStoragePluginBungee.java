package net.cubekrowd.eventstorageapi.bungeecord;

import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.util.*;
import java.io.*;
import java.util.logging.Level;
import net.md_5.bungee.*;
import net.md_5.bungee.api.*;
import net.md_5.bungee.api.chat.*;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.api.plugin.*;
import net.md_5.bungee.config.*;
import net.md_5.bungee.event.*;
import org.bson.Document;
import net.cubekrowd.eventstorageapi.api.*;
import net.cubekrowd.eventstorageapi.api.event.*;
import net.cubekrowd.eventstorageapi.common.*;
import net.cubekrowd.eventstorageapi.common.command.*;
import net.cubekrowd.eventstorageapi.common.storage.*;
import net.cubekrowd.eventstorageapi.bungeecord.event.*;
import lombok.*;

public class EventStoragePluginBungee extends Plugin implements EventStoragePlugin, Listener {
    private Configuration config;

    @Override
    public void onEnable() {
        saveDefaultConfig("config.yml");
        config = loadConfig("config.yml");

        if (config.getInt("version") != 1) {
            getLogger().severe(ChatColor.RED +
                               "WRONG CONFIGURATION VERSION. PLEASE BACKUP AND DELETE THE CONFIG.YML. The latest version will be generated automatically during next restart if you have moved the old one. Disabling...");
            onDisable();
            return;
        }

        if(config.getBoolean("mongodb.enabled")) {
            EventStorageAPI.registerInit(new StorageMongoDB(this, config.getString("mongodb.host"), config.getInt("mongodb.port"), config.getString("mongodb.database"), config.getString("mongodb.username"), config.getString("mongodb.password")));
        } else {
            EventStorageAPI.registerInit(new StorageYAML(this, getDataFolder(), (String s) -> getResourceAsStream(s)));
        }

        EventStorageAPI.getStorage().open();

        getProxy().getPluginManager().registerCommand(this, new BungeeCommand(this));
        getProxy().getPluginManager().registerListener(this, this);
    }

    @Override
    public void onDisable() {
        EventStorageAPI.getStorage().close();
    }

    public void saveDefaultConfig(String name) {
        getDataFolder().mkdirs();
        var configFile = new File(getDataFolder(), name);
        try {
            Files.copy(getResourceAsStream(name), configFile.toPath());
        } catch (FileAlreadyExistsException e) {
            // ignore
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Can't save default config " + name, e);
        }
    }

    public Configuration loadConfig(String name) {
        var configProvider = ConfigurationProvider.getProvider(YamlConfiguration.class);
        try {
            return configProvider.load(new File(getDataFolder(), name));
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Can't load config " + name, e);
            return new Configuration();
        }
    }


    @EventHandler
    public void onServerConnectedEvent(ServerConnectedEvent e) {
        if(isDebug()) {
            e.getPlayer().sendMessage(new TextComponent(TextComponent.fromLegacyText(ESAPICommand.PREFIX + ChatColor.RED + "Debug-mode is activated! Please turn off debug mode in production!")));
        }
    }

    @Override
    public List<String> getAuthors() {
        return Arrays.asList(getDescription().getAuthor().replaceAll(",", "").split(" "));
    }

    @Override
    public String getVersion() {
        return getDescription().getVersion();
    }

    @Override
    public boolean isDebug() {
        return config.getBoolean("debug");
    }

    @Override
    public List<Messagable> getDebuggers() {
        return Arrays.asList(new Messagable() {
            @Override
            public void sendMessage(BaseComponent component) {
                getProxy().getConsole().sendMessage(component);
            }

            @Override
            public boolean hasPermission(String permission) {
                return true;
            }

        });
    }

    @Override
    public void callEvent(ESAPIEvent event, Object... data) {
        switch (event) {
        case STORAGE_ENTRY_ADDED:
            getProxy().getPluginManager().callEvent(new BungeeStorageEntryAddedEvent((EventEntry) data[0]));
            break;
        default:
            throw new RuntimeException("Unsupported event: " + event.name());
        }
    }

}
