package net.cubekrowd.eventstorageapi.bungeecord.event;

import net.cubekrowd.eventstorageapi.api.*;
import net.cubekrowd.eventstorageapi.api.event.*;
import net.md_5.bungee.api.plugin.Event;
import lombok.*;

@RequiredArgsConstructor
public class BungeeStorageEntryAddedEvent extends Event implements StorageEntryAddedEvent {

    @Getter
    private final EventEntry entry;

}
