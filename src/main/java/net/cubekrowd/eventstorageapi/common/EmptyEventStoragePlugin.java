package net.cubekrowd.eventstorageapi.common;

import java.util.*;
import net.cubekrowd.eventstorageapi.api.event.*;
import net.cubekrowd.eventstorageapi.common.command.*;

public class EmptyEventStoragePlugin implements EventStoragePlugin {

    @Override
    public List<String> getAuthors() {
        return null;
    }

    @Override
    public String getVersion() {
        return null;
    }

    @Override
    public void callEvent(ESAPIEvent event, Object... data) {
    }

    @Override
    public boolean isDebug() {
        return false;
    }

    @Override
    public List<Messagable> getDebuggers() {
        return new ArrayList<>();
    }

}
