package net.cubekrowd.eventstorageapi.common;

import java.util.*;
import net.cubekrowd.eventstorageapi.api.event.*;
import net.cubekrowd.eventstorageapi.common.command.*;

public interface EventStoragePlugin {

    public boolean isDebug();

    public List<Messagable> getDebuggers();

    public List<String> getAuthors();

    public String getVersion();

    public void callEvent(ESAPIEvent event, Object... data);

}
