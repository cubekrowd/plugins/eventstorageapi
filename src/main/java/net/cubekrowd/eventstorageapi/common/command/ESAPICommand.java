package net.cubekrowd.eventstorageapi.common.command;

import java.time.*;
import java.time.format.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.*;
import net.cubekrowd.eventstorageapi.api.*;
import net.cubekrowd.eventstorageapi.common.*;
import lombok.*;

public interface ESAPICommand {

    public static final String PERMISSION_ADMIN = "eventstorageapi.admin";
    public static final String PREFIX = ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "ESAPI" + ChatColor.DARK_GRAY + "] ";

    default public void executeCommand(EventStoragePlugin plugin, Messagable sender, String[] args) {
        // print help
        if (args.length == 0 || (args.length != 0 && args[0].equalsIgnoreCase("help"))) {
            sender.sendMessage(getVersion(plugin.getVersion()));
            if (sender.hasPermission(PERMISSION_ADMIN)) {
                getHelp().forEach(m -> sender.sendMessage(m));
            }
            return;
        }

        if (args[0].equalsIgnoreCase("plugins")) {
            if (!sender.hasPermission(PERMISSION_ADMIN)) {
                return;
            }
            EventStorageAPI.getStorage().getPlugins();
            return;
        }

        if (args[0].equalsIgnoreCase("events")) {
            if (!sender.hasPermission(PERMISSION_ADMIN)) {
                return;
            }
            if(args.length == 2) {
                EventStorageAPI.getStorage().getEventTypes(args[1]);
                return;
            }
        }

        if (args[0].equalsIgnoreCase("count")) {
            if (!sender.hasPermission(PERMISSION_ADMIN)) {
                return;
            }
            if(args.length == 3) {
                getCount(args[1], args[2]).forEach(m -> sender.sendMessage(m));
                return;
            }
            if(args.length == 5) {
                getCount(args[1], args[2], Long.parseLong(args[3]), Long.parseLong(args[4])).forEach(m -> sender.sendMessage(m));
                return;
            }
        }

        if (args[0].equalsIgnoreCase("time")) {
            if (!sender.hasPermission(PERMISSION_ADMIN)) {
                return;
            }
            if(args.length == 1) {
                getTime(System.currentTimeMillis() + "").forEach(m -> sender.sendMessage(m));
                return;
            }
            if(args.length == 2) {
                getTime(args[1]).forEach(m -> sender.sendMessage(m));
                return;
            }
            if(args.length == 3) {
                getTime(args[1], args[2]).forEach(m -> sender.sendMessage(m));
                return;
            }
        }

        if (args[0].equalsIgnoreCase("find")) {
            if (!sender.hasPermission(PERMISSION_ADMIN)) {
                return;
            }
            if(args.length == 3) {
                getFind(args[1], args[2]).forEach(m -> sender.sendMessage(m));
                return;
            }
            if(args.length == 5) {
                getFind(args[1], args[2], args[3], args[4]).forEach(m -> sender.sendMessage(m));
                return;
            }
            if(args.length == 6) {
                getFind(args[1], args[2], args[3], args[4], args[5]).forEach(m -> sender.sendMessage(m));
                return;
            }
        }

        if (args[0].equalsIgnoreCase("show") && args.length == 4) {
            if (!sender.hasPermission(PERMISSION_ADMIN)) {
                return;
            }
            getShow(args[1], args[2], Long.parseLong(args[3])).forEach(m -> sender.sendMessage(m));
            return;
        }

        if (args[0].equalsIgnoreCase("add") && args.length == 5) {
            if (!sender.hasPermission(PERMISSION_ADMIN)) {
                return;
            }
            if (!plugin.isDebug()) {
                sender.sendMessage(t(PREFIX + ChatColor.RED + "This command is only available in debug-mode!"));
                return;
            }
            long timestamp;
            try {
                timestamp = Long.parseLong(args[3]);
            } catch(Exception e) {
                sender.sendMessage(t(PREFIX + ChatColor.RED + "Invalid time! Time argument is not a number."));
                return;
            }

            Map<String, String> data = stringToDataMap(args[4]);
            if(data == null) {
                sender.sendMessage(t(PREFIX + ChatColor.RED + "Invalid data! Missing '=' in data part."));
                return;
            }

            EventEntry ee;
            try {
                ee = new EventEntry(args[1], args[2], timestamp, data);
            } catch(Exception e) {
                sender.sendMessage(t(PREFIX + ChatColor.RED + e.getMessage()));
                return;
            }
            EventStorageAPI.getStorage().addEntry(ee);
            return;
        }

        if (args[0].equalsIgnoreCase("credits")) {
            getCredits(plugin.getAuthors(), plugin.getVersion()).forEach(m -> sender.sendMessage(m));
            return;
        }

        // unknown command, print help
        sender.sendMessage(t(PREFIX + ChatColor.RED + "Invalid command!"));
        sender.sendMessage(getVersion(plugin.getVersion()));
        getHelp().forEach(m -> sender.sendMessage(m));
    }

    default public TextComponent t(@NonNull String s) {
        return new TextComponent(TextComponent.fromLegacyText(s));
    }

    default public TextComponent getVersion(@NonNull String version) {
        return t(PREFIX + ChatColor.GREEN + "Running version " + ChatColor.AQUA + version);
    }

    default public List<TextComponent> getCount(@NonNull String plugin, @NonNull String type) {
        return getCount(plugin, type, -1, -1);
    }

    default public List<TextComponent> getCount(@NonNull String plugin, @NonNull String type, long startTime, long endTime) {
        plugin = plugin.toLowerCase().replaceAll("[^a-z0-9_]", "");
        type = type.toLowerCase().replaceAll("[^a-z0-9_]", "");
        List<TextComponent> output = new ArrayList<>();
        output.add(t(PREFIX + ChatColor.GREEN + "Executing command:"));
        output.add(t(PREFIX + ChatColor.DARK_GRAY + "Query: " + ChatColor.GOLD + "COUNT" + ChatColor.DARK_GRAY + " Plugin: " + ChatColor.GOLD + plugin + ChatColor.DARK_GRAY + " Type: " + ChatColor.GOLD + type + (startTime != -1 && endTime != -1 ? ChatColor.DARK_GRAY + " Time: " + ChatColor.GOLD + startTime + ChatColor.DARK_GRAY + "->" + ChatColor.GOLD + endTime : "")));
        output.add(t(PREFIX + ChatColor.DARK_GRAY + "Result: " + ChatColor.DARK_AQUA + EventStorageAPI.getStorage().getEntryStream(plugin, type, startTime, endTime).count()));
        return output;
    }

    default public List<TextComponent> getFind(String plugin, String type) {
        return getFind(plugin, type, "-1", "-1");
    }

    default public List<TextComponent> getFind(String plugin, String type, String startTime, String endTime) {
        return getFind(plugin, type, startTime, endTime, "");
    }

    default public List<TextComponent> getFind(@NonNull String plugin, @NonNull String type, @NonNull String sstartTime, @NonNull String sendTime, @NonNull String sfilter) {
        Map<String, String> filter = stringToDataMap(sfilter);
        if(filter == null) {
            return Arrays.asList(t(PREFIX + ChatColor.RED + "Invalid data! Missing '=' in data part."));
        }
        plugin = plugin.toLowerCase().replaceAll("[^a-z0-9_]", "");
        List<String> types = Arrays.asList(type.toLowerCase().replaceAll("[^a-z0-9_,]", "").split(","));
        long startTime;
        try {
            startTime = Long.parseLong(sstartTime);
        } catch(NumberFormatException e) {
            return Arrays.asList(t(PREFIX + ChatColor.RED + "Invalid start time number!"));
        }
        long endTime;
        try {
            endTime = Long.parseLong(sendTime);
        } catch(NumberFormatException e) {
            return Arrays.asList(t(PREFIX + ChatColor.RED + "Invalid end time number!"));
        }

        List<TextComponent> output = new ArrayList<>();
        output.add(t(PREFIX + ChatColor.GREEN + "Executing command:"));
        output.add(t(PREFIX + ChatColor.DARK_GRAY + "Query: " + ChatColor.GOLD + "FIND" + ChatColor.DARK_GRAY + " Plugin: " + ChatColor.GOLD + plugin + ChatColor.DARK_GRAY + " Type: " + ChatColor.GOLD + String.join(",", types) + ChatColor.DARK_GRAY + (startTime != -1 && endTime != -1 ? " Time: " + ChatColor.GOLD + startTime + ChatColor.DARK_GRAY + "->" + ChatColor.GOLD + endTime : "")));
        output.add(t(PREFIX + ChatColor.DARK_GRAY + "Data:"));
        filter.entrySet().forEach(e -> output.add(t(PREFIX + ChatColor.DARK_GRAY + "- " + e.getKey() + ": " + ChatColor.DARK_AQUA+ e.getValue())));
        output.add(t(PREFIX + ChatColor.DARK_GRAY + "Result:"));

        EventStorageAPI.getStorage().getEntryStream(plugin, types, startTime, endTime, filter).limit(20).forEach(ee -> output.add(t(PREFIX + ChatColor.DARK_GRAY + "- " + ChatColor.DARK_AQUA + ee.getPlugin() + ":" + ee.getType() + ":" + ee.getTime())));
        output.add(t(PREFIX + ChatColor.GRAY + "Note: FIND queries are limited at 20 results per search."));

        return output;
    }

    default public List<TextComponent> getShow(@NonNull String plugin, @NonNull String type, long time) {
        plugin = plugin.toLowerCase().replaceAll("[^a-z0-9_]", "");
        List<TextComponent> output = new ArrayList<>();
        output.add(t(PREFIX + ChatColor.GREEN + "Executing command:"));
        output.add(t(PREFIX + ChatColor.DARK_GRAY + "Query: " + ChatColor.GOLD + "SHOW" + ChatColor.DARK_GRAY + " Plugin: " + ChatColor.GOLD + plugin + ChatColor.DARK_GRAY + " Type: " + ChatColor.GOLD + type + ChatColor.DARK_GRAY + " Time: " + ChatColor.GOLD + time));
        output.add(t(PREFIX + ChatColor.DARK_GRAY + "Result:"));

        EventStorageAPI.getStorage().getEntryStream(plugin, type, time, time + 1).forEach(ee -> {
            output.add(t(PREFIX + ChatColor.DARK_GRAY + "  Plugin: " + ChatColor.DARK_AQUA + ee.getPlugin() + ChatColor.DARK_GRAY + " Type: " + ChatColor.DARK_AQUA + ee.getType() + ChatColor.DARK_GRAY + " Time:" + ChatColor.DARK_AQUA + ee.getTime()));
            output.add(t(PREFIX + ChatColor.DARK_GRAY + "    Data:"));
            ee.getData().entrySet().forEach(me -> output.add(t(PREFIX + ChatColor.DARK_GRAY + "    - " + me.getKey() + ": " + ChatColor.DARK_AQUA + me.getValue())));
        });

        return output;
    }

    default public List<TextComponent> getTime(String time) {
        return getTime(null, time);
    }

    default public List<TextComponent> getTime(String date, String time) {
        long timestamp = -1;
        try {
            timestamp = Long.parseLong(time);
        } catch(NumberFormatException e) {
            try {
                if(date == null) {
                    timestamp = Instant.parse(time + "T00:00:00.000Z").toEpochMilli();
                } else {
                    timestamp = Instant.parse(date + "T" + time + "Z").toEpochMilli();
                }
            } catch(DateTimeParseException e2) {
                return Arrays.asList(t(PREFIX + ChatColor.RED + "Invalid time!"));
            }
        }

        List<TextComponent> output = new ArrayList<>();
        output.add(t(PREFIX + ChatColor.GREEN + "Time information:"));
        output.add(t(PREFIX + ChatColor.DARK_GRAY + "Timestamp: " + ChatColor.GOLD + timestamp));

        Instant in = Instant.ofEpochMilli(timestamp);

        output.add(t(PREFIX + ChatColor.DARK_GRAY + "Date: " + ChatColor.GOLD + in.toString().split("T")[0] + ChatColor.DARK_GRAY + " Time: " + ChatColor.GOLD + in.toString().split("T")[1].split("Z")[0]));

        return output;
    }

    default public List<TextComponent> getHelp() {
        return Arrays.asList("help", "time [time] / [date [time]]", "plugins", "events <plugin>", "count <plugin> <eventType> [<startTime> <endTime>]", "find <plugin> <eventType> [<startTime> <endTime>] [data]", "show <plugin> <eventType> <time>", "add <plugin> <eventType> <time> <data>", "credits").stream().map(s -> t(PREFIX + ChatColor.DARK_GRAY + "/esapi " + ChatColor.DARK_AQUA + s)).collect(Collectors.toList());
    }

    default public List<TextComponent> getCredits(@NonNull List<String> authors, @NonNull String version) {
        List<TextComponent> credits = new ArrayList<>();
        credits.add(t(PREFIX + ChatColor.GREEN + "Credits:"));
        credits.add(t(PREFIX + ChatColor.DARK_GRAY + "Authors: " + ChatColor.GOLD + String.join(", ", authors)));
        credits.add(t(PREFIX + ChatColor.DARK_GRAY + "Version: " + ChatColor.GOLD + version));
        credits.add(t(PREFIX + ChatColor.DARK_GRAY + "Source code: " + ChatColor.GOLD + "https://gitlab.com/cubekrowd/plugins/eventstorageapi"));
        credits.add(t(PREFIX + ChatColor.DARK_AQUA + "This is a free open-source plugin, originally developed for CubeKrowd. If you like it then please leave a review on the Spigot page. Thank you."));
        return credits;
    }

    default public Map<String, String> stringToDataMap(String s) {
        Map<String, String> data = new HashMap<>();
        if(s.length() == 0) {
            return data;
        }
        String[] parts = s.split(",");
        String[] split;
        try {
            for(String part : parts) {
                split = part.split("=");
                data.put(split[0], split[1]);
            }
        } catch(IndexOutOfBoundsException e) {
            return null;
        }
        return data;
    }
}
