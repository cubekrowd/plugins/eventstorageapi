package net.cubekrowd.eventstorageapi.common.command;

import java.util.*;
import net.md_5.bungee.api.chat.*;
import lombok.*;

public interface Messagable {

    public void sendMessage(BaseComponent component);

    default public void sendMessage(@NonNull List<BaseComponent> components) {
        components.forEach(this::sendMessage);
    }

    default public void sendMessage(@NonNull String message) {
        sendMessage(new TextComponent(TextComponent.fromLegacyText(message)));
    }

    public boolean hasPermission(String permissions);

}
