package net.cubekrowd.eventstorageapi.common.storage;

import java.io.*;
import java.util.*;
import java.util.stream.*;
import net.md_5.bungee.api.ChatColor;
import net.cubekrowd.eventstorageapi.api.*;
import net.cubekrowd.eventstorageapi.api.event.*;
import net.cubekrowd.eventstorageapi.common.*;
import net.cubekrowd.eventstorageapi.common.command.*;
import lombok.*;

@RequiredArgsConstructor
public abstract class AbstractStorageImpl extends EventStorage {

    @Getter @NonNull private final EventStoragePlugin plugin;

    private String stackWalk() {
        List<StackWalker.StackFrame> framesAfterSkip = StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE).walk((s) ->s.skip(2).collect(Collectors.toList()));
        return framesAfterSkip.get(0).getClassName();
    }

    public boolean checkStackWalk(@NonNull String sw) {
        return sw.startsWith("net.cubekrowd.eventstorageapi.common.command");
    }

    @Override public final void open() {
        RuntimeException e = null;
        try {
            open2();
        } catch (RuntimeException e2) {
            e = e2;
        }
        String sw = stackWalk();
        if(getPlugin().isDebug() || checkStackWalk(sw)) {
            for(Messagable sender : getPlugin().getDebuggers()) {
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.GREEN + "Executing query: " + ChatColor.GRAY + sw);
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.DARK_GRAY + "Query: " + ChatColor.GOLD + "OPEN");
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.DARK_GRAY + "Result: " + ChatColor.DARK_AQUA + (e == null ? "OK" : "ERROR"));
            }
        }
        if(e != null) {
            throw e;
        }
    }

    @Override public final void close() {
        RuntimeException e = null;
        try {
            close2();
        } catch (RuntimeException e2) {
            e = e2;
        }
        String sw = stackWalk();
        if(getPlugin().isDebug() || checkStackWalk(sw)) {
            for(Messagable sender : getPlugin().getDebuggers()) {
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.GREEN + "Executing query: " + ChatColor.GRAY + sw);
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.DARK_GRAY + "Query: " + ChatColor.GOLD + "CLOSE");
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.DARK_GRAY + "Result: " + ChatColor.DARK_AQUA + (e == null ? "OK" : "ERROR"));
            }
        }
        if(e != null) {
            throw e;
        }
    }

    @Override public final List<String> getPlugins() {
        List<String> plugins = getPlugins2();
        String sw = stackWalk();
        if(getPlugin().isDebug() || checkStackWalk(sw)) {
            for(Messagable sender : getPlugin().getDebuggers()) {
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.GREEN + "Executing query: " + ChatColor.GRAY + sw);
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.DARK_GRAY + "Query: " + ChatColor.GOLD + "LIST_PLUGINS");
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.DARK_GRAY + "Result: " + ChatColor.DARK_AQUA + String.join(", ", plugins));
            }
        }
        return plugins;
    }

    @Override public final List<String> getEventTypes(@NonNull String plugin) {
        plugin = plugin.toLowerCase().replaceAll("[^a-z0-9_]", "");

        List<String> types = getEventTypes2(plugin);
        String sw = stackWalk();
        if(getPlugin().isDebug() || checkStackWalk(sw)) {
            for(Messagable sender : getPlugin().getDebuggers()) {
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.GREEN + "Executing query: " + ChatColor.GRAY + sw);
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.DARK_GRAY + "Query: " + ChatColor.GOLD + "LIST_EVENT_TYPES " + ChatColor.DARK_GRAY + "Plugin: " + ChatColor.GOLD + plugin);
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.DARK_GRAY + "Result: " + ChatColor.DARK_AQUA + String.join(", ", types));
            }
        }
        return types;
    }
    @Override public final void addEntry(@NonNull EventEntry ee) {
        addEntry2(ee);

        String sw = stackWalk();
        if(getPlugin().isDebug() || checkStackWalk(sw)) {
            for(Messagable sender : getPlugin().getDebuggers()) {
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.GREEN + "Executing query: " + ChatColor.GRAY + sw);
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.DARK_GRAY + "Query: " + ChatColor.GOLD + "ADD_ENTRY");
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.DARK_GRAY + "Data:");
                printEntry(sender, ee);
            }
        }
    }
    @Override public final List<EventEntry> getEntryList(@NonNull String plugin, @NonNull List<String> types, long startTime, long endTime, Map<String, String> filter) {
        plugin = plugin.toLowerCase().replaceAll("[^a-z0-9_]", "");
        types = types.stream().map(s -> s.toLowerCase().replaceAll("[^a-z0-9_]", "")).collect(Collectors.toList());
        List<EventEntry> list = getEntryList2(plugin, types, startTime, endTime, filter);

        String sw = stackWalk();
        if(getPlugin().isDebug() || checkStackWalk(sw)) {
            for(Messagable sender : getPlugin().getDebuggers()) {
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.GREEN + "Executing query: " + ChatColor.GRAY + sw);
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.DARK_GRAY + "Query: " + ChatColor.GOLD + "ENTRY_LIST" + ChatColor.DARK_GRAY + " Plugin: " + ChatColor.GOLD + plugin + ChatColor.DARK_GRAY + " Types: " + ChatColor.GOLD + String.join(",", types) + (startTime != -1 && endTime == -1 ? ChatColor.DARK_GRAY + " Time: " + ChatColor.GOLD + startTime + ChatColor.DARK_GRAY + "->" + ChatColor.GOLD + endTime : ""));
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.DARK_GRAY + "Result:");
                list.forEach(ee -> printEntry(sender, ee));
            }
        }

        return list;
    }
    @Override public final Stream<EventEntry> getEntryStream(@NonNull String plugin, @NonNull List<String> types, long startTime, long endTime, Map<String, String> filter) {
        plugin = plugin.toLowerCase().replaceAll("[^a-z0-9_]", "");
        types = types.stream().map(s -> s.toLowerCase().replaceAll("[^a-z0-9_]", "")).collect(Collectors.toList());
        Stream<EventEntry> stream = getEntryStream2(plugin, types, startTime, endTime, filter);

        String sw = stackWalk();
        if(getPlugin().isDebug() || checkStackWalk(sw)) {
            for(Messagable sender : getPlugin().getDebuggers()) {
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.GREEN + "Executing query: " + ChatColor.GRAY + sw);
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.DARK_GRAY + "Query: " + ChatColor.GOLD + "ENTRY_STREAM" + ChatColor.DARK_GRAY + " Plugin: " + ChatColor.GOLD + plugin + ChatColor.DARK_GRAY + " Types: " + ChatColor.GOLD + String.join(",", types) + (startTime != -1 && endTime == -1 ? ChatColor.DARK_GRAY + " Time: " + ChatColor.GOLD + startTime + ChatColor.DARK_GRAY + "->" + ChatColor.GOLD + endTime : ""));
                sender.sendMessage(ESAPICommand.PREFIX + ChatColor.DARK_GRAY + "Result: " + ChatColor.DARK_AQUA + "???");
            }
        }

        return stream;
    }

    protected abstract void open2();
    protected abstract void close2();
    protected abstract List<String> getPlugins2();
    protected abstract List<String> getEventTypes2(@NonNull String plugin);
    protected abstract void addEntry2(@NonNull EventEntry eventEntry);
    protected abstract List<EventEntry> getEntryList2(@NonNull String plugin, @NonNull List<String> types, long startTime, long endTime, Map<String, String> filter);
    protected abstract Stream<EventEntry> getEntryStream2(@NonNull String plugin, @NonNull List<String> types, long startTime, long endTime, Map<String, String> filter);

    private void printEntry(Messagable m, EventEntry e) {
        m.sendMessage(ESAPICommand.PREFIX + ChatColor.DARK_GRAY + "  Plugin: " + ChatColor.DARK_AQUA + e.getPlugin() + ChatColor.DARK_GRAY + " Type: " + ChatColor.DARK_AQUA + e.getType() + ChatColor.DARK_GRAY + " Time: " + ChatColor.DARK_AQUA + e.getTime());
        m.sendMessage(ESAPICommand.PREFIX + ChatColor.DARK_GRAY + "    Data:");
        for(Map.Entry<String, String> de : e.getData().entrySet()) {
            m.sendMessage(ESAPICommand.PREFIX + ChatColor.DARK_GRAY + "    - " + de.getKey() + ": " + ChatColor.DARK_AQUA + de.getValue());
        }
    }

}
