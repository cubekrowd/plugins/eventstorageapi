package net.cubekrowd.eventstorageapi.common.storage;

import java.io.*;
import java.util.*;
import java.util.stream.*;
import com.google.common.io.*;
import com.google.common.base.Preconditions;
import com.mongodb.*;
import com.mongodb.client.*;
import org.bson.Document;
import net.cubekrowd.eventstorageapi.api.*;
import net.cubekrowd.eventstorageapi.api.event.*;
import net.cubekrowd.eventstorageapi.common.*;
import lombok.*;

public class StorageMongoDB extends AbstractStorageImpl {

    private MongoClient mongoClient;

    private final String host, database, username, password;
    private final int port;

    public StorageMongoDB(@NonNull EventStoragePlugin plugin, @NonNull String host, int port, @NonNull String database, @NonNull String username, @NonNull String password) {
        super(plugin);
        this.host = host;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;
    }

    @Override
    protected void open2() {
        var mcsb = MongoClientSettings.builder()
            .applyToClusterSettings(b -> b.hosts(List.of(new ServerAddress(host, port))));
        if (!username.equals("")) {
            mcsb.credential(MongoCredential.createCredential(username, database, password.toCharArray()));
        }
        mongoClient = MongoClients.create(mcsb.build());
    }

    @Override
    protected void close2() {
        mongoClient.close();
        mongoClient = null;
    }

    @Override
    protected List<String> getPlugins2() {
        Preconditions.checkNotNull(mongoClient, "Storage is not opened");
        return StreamSupport.stream(mongoClient.getDatabase(database).listCollectionNames().spliterator(), false).filter(n -> n.startsWith("storage_")).map(n -> n.split("_")[1]).collect(Collectors.toList());
    }

    @Override
    protected List<String> getEventTypes2(String plugin) {
        Preconditions.checkNotNull(mongoClient, "Storage is not opened");
        if(!getPlugins().contains(plugin)) {
            return new ArrayList<>();
        }
        DistinctIterable<String> it = getCollection(plugin).distinct("k", String.class);
        return StreamSupport.stream(it.spliterator(), false).collect(Collectors.toList());
    }

    @Override
    protected void addEntry2(@NonNull EventEntry eventEntry) {
        Preconditions.checkNotNull(mongoClient, "Storage is not opened");
        Document doc = new Document();
        doc.append("k", eventEntry.getType());
        doc.append("t", eventEntry.getTime());

        Document data = new Document();
        eventEntry.getData().entrySet().forEach(e -> data.append(e.getKey(), e.getValue()));
        doc.append("d", data);

        getCollection(eventEntry.getPlugin()).insertOne(doc);

        // call added event
        getPlugin().callEvent(ESAPIEvent.STORAGE_ENTRY_ADDED, eventEntry);
    }

    @Override
    protected List<EventEntry> getEntryList2(@NonNull String plugin, @NonNull List<String> types, long startTime, long endTime, Map<String, String> filter) {
        Preconditions.checkNotNull(mongoClient, "Storage is not opened");
        return getEntryStream(plugin, types, startTime, endTime, filter).collect(Collectors.toList());
    }

    @Override
    protected Stream<EventEntry> getEntryStream2(@NonNull String plugin2, @NonNull List<String> types, long startTime, long endTime, Map<String, String> filter) {
        Preconditions.checkNotNull(mongoClient, "Storage is not opened");
        String plugin = plugin2.toLowerCase().replaceAll("[^a-z0-9]", "");
        if(plugin.isEmpty()) {
            throw new IllegalArgumentException("Plugin name cannot be empty");
        }
        for(String type : types) {
            if(type.isEmpty()) {
                throw new IllegalArgumentException("Event type cannot be empty");
            }
        }
        FindIterable<Document> it = getCollection(plugin).find(createQuery(types, startTime, endTime, filter));
        return StreamSupport.stream(it.spliterator(), false).map( ee -> documentToEventEntry(plugin, ee));
    }

    private MongoCollection<Document> getCollection(String plugin) {
        return mongoClient.getDatabase(database).getCollection("storage_" + plugin.toLowerCase().replaceAll("[^a-z0-9]", ""));
    }

    private EventEntry documentToEventEntry(String plugin, Document doc) {
        Map<String, String> map = doc.get("d", Document.class).entrySet().stream().collect(Collectors.toMap((e -> e.getKey()), (e -> (String) e.getValue())));
        return new EventEntry(plugin, doc.getString("k"), doc.getLong("t"), Collections.unmodifiableMap(map));
    }

    private Document createQuery(List<String> types, long startTime, long endTime, Map<String, String> filter) {
        Document search = new Document("k", new Document("$in", types));
        if(startTime != -1 || endTime != -1) {
            Document time = new Document();
            if(startTime != -1) {
                time.append("$gte", startTime);
            }
            if(endTime != -1) {
                time.append("$lt", endTime);
            }
            search.append("t", time);
        }
        filter.entrySet().forEach(e -> search.append("d." + e.getKey(), e.getValue()));
        return search;
    }

}
