package net.cubekrowd.eventstorageapi.common.storage;

import java.io.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import net.md_5.bungee.config.*;
import net.cubekrowd.eventstorageapi.api.*;
import net.cubekrowd.eventstorageapi.api.event.*;
import net.cubekrowd.eventstorageapi.common.*;
import com.google.common.io.*;
import com.google.common.base.Preconditions;
import lombok.*;

public class StorageYAML extends AbstractStorageImpl {

    private final File dataFolder;
    private Configuration data;

    public StorageYAML(@NonNull EventStoragePlugin plugin, @NonNull File dataFolder, @NonNull Function<String, InputStream> iss) {
        super(plugin);
        this.dataFolder = dataFolder;
    }
    public StorageYAML(@NonNull EventStoragePlugin plugin, @NonNull File dataFolder) {
        super(plugin);
        this.dataFolder = dataFolder;
    }

    @SneakyThrows
    private void createConfig(File dataFile) {
        if (dataFile.exists()) {
            return;
        }
        dataFile.createNewFile();
        @Cleanup PrintWriter ofs = new PrintWriter(dataFile);
        ofs.println("# EventStorageAPI yaml storage");
        ofs.println("# DO NOT EDIT VERSION");
        ofs.println("version: 1");
        ofs.println("");
        ofs.println("# Events");
        ofs.println("events: {}");
    }

    @Override
    protected void open2() {
        File dataFile = new File(dataFolder, "data.yml");
        createConfig(dataFile);
        try {
            data = ConfigurationProvider.getProvider(YamlConfiguration.class).load(
                       dataFile);
        } catch (IOException e) {
            throw new RuntimeException("Unable to read data file", e);
        }
    }

    @Override
    protected void close2() {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(data,
                    new File(dataFolder, "data.yml"));
        } catch (IOException e) {
            throw new RuntimeException("Unable to save data file", e);
        }
    }

    @Override
    protected List<String> getPlugins2() {
        Preconditions.checkNotNull(data, "Storage is not opened");
        Configuration events = data.getSection("events");
        return events.getKeys().stream().map(key -> events.getString(key + ".plugin")).distinct().collect(Collectors.toList());
    }

    @Override
    protected List<String> getEventTypes2(String plugin) {
        Preconditions.checkNotNull(data, "Storage is not opened");
        Configuration events = data.getSection("events");
        return events.getKeys().stream().filter(key -> events.getString(key + ".plugin").equals(plugin)).map(key -> events.getString(key + ".type")).distinct().collect(Collectors.toList());
    }

    @Override
    protected void addEntry2(@NonNull EventEntry eventEntry) {
        Preconditions.checkNotNull(data, "Storage is not opened");
        int id = data.getSection("events").getKeys().size();

        data.set("events." + id + ".plugin", eventEntry.getPlugin());
        data.set("events." + id + ".type", eventEntry.getType());
        data.set("events." + id + ".time", eventEntry.getTime());
        data.set("events." + id + ".data", eventEntry.getData());

        // call added event
        getPlugin().callEvent(ESAPIEvent.STORAGE_ENTRY_ADDED, eventEntry);
    }

    @Override
    protected List<EventEntry> getEntryList2(@NonNull String plugin2, @NonNull List<String> types, long startTime, long endTime, Map<String, String> filter) {
        Preconditions.checkNotNull(data, "Storage is not opened");
        final String plugin = plugin2.toLowerCase().replaceAll("[^a-z0-9]", "");
        if(plugin.isEmpty()) {
            throw new IllegalArgumentException("Plugin name cannot be empty");
        }
        for(String type : types) {
            if(type.isEmpty()) {
                throw new IllegalArgumentException("Event type cannot be empty");
            }
        }
        Configuration events = data.getSection("events");

        return events.getKeys().stream()
               .filter(key -> events.getString(key + ".plugin").equals(plugin)) // filter plugin
               .filter(key -> types.contains(events.getString(key + ".type"))) // filter type
               .filter(key -> startTime == -1 || events.getLong(key + ".time") >= startTime) // filter start time
               .filter(key -> endTime == -1 || events.getLong(key + ".time") < endTime) // filter stop time
        .filter(key -> { // filter data
            for(String fs : filter.keySet()) {
                if(!filter.get(fs).equals(events.getString(key + ".data." + fs))) {
                    return false;
                }
            }
            return true;
        }).map(key -> {
            Configuration eventData = events.getSection(key + ".data");
            return new EventEntry(events.getString(key + ".plugin"), events.getString(key + ".type"), events.getLong(key + ".time"), Collections.unmodifiableMap(eventData.getKeys().stream().collect(Collectors.toMap(k -> k, k -> eventData.getString(k)))));
        }).collect(Collectors.toList());
    }

    @Override
    protected Stream<EventEntry> getEntryStream2(@NonNull String plugin, @NonNull List<String> types, long startTime, long endTime, Map<String, String> filter) {
        Preconditions.checkNotNull(data, "Storage is not opened");
        // The reason we cast it to a list first is because the YAML data can be changed while it's read later
        return getEntryList(plugin, types, startTime, endTime, filter).stream();
    }

}
