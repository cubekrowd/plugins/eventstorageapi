package net.cubekrowd.eventstorageapi.spigot;

import java.util.*;
import org.bukkit.*;
import org.bukkit.event.*;
import org.bukkit.event.player.*;
import org.bukkit.plugin.java.*;
import org.bson.Document;
import net.md_5.bungee.api.chat.*;
import net.cubekrowd.eventstorageapi.api.*;
import net.cubekrowd.eventstorageapi.api.event.*;
import net.cubekrowd.eventstorageapi.common.*;
import net.cubekrowd.eventstorageapi.common.command.*;
import net.cubekrowd.eventstorageapi.common.storage.*;
import net.cubekrowd.eventstorageapi.spigot.event.*;

public class EventStoragePluginSpigot extends JavaPlugin implements EventStoragePlugin, Listener  {

    @Override
    public void onEnable() {
        saveDefaultConfig();

        if (getConfig().getInt("version") != 1) {
            getLogger().severe(ChatColor.RED +
                               "WRONG CONFIGURATION VERSION. PLEASE BACKUP AND DELETE THE CONFIG.YML. The latest version will be generated automatically during next restart if you have moved the old one. Disabling...");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        if(getConfig().getBoolean("mongodb.enabled")) {
            EventStorageAPI.registerInit(new StorageMongoDB(this, getConfig().getString("mongodb.host"), getConfig().getInt("mongodb.port"), getConfig().getString("mongodb.database"), getConfig().getString("mongodb.username"), getConfig().getString("mongodb.password")));
        } else {
            EventStorageAPI.registerInit(new StorageYAML(this, getDataFolder(), (s) -> getClass().getClassLoader().getResourceAsStream(s)));
        }

        EventStorageAPI.getStorage().open();

        getCommand("esapi").setExecutor(new SpigotCommand(this));
        getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        EventStorageAPI.getStorage().close();
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        if(isDebug()) {
            e.getPlayer().spigot().sendMessage(new TextComponent(TextComponent.fromLegacyText(ESAPICommand.PREFIX + ChatColor.RED + "Debug-mode is activated! Please turn off debug mode in production!")));
        }
    }

    @Override
    public List<String> getAuthors() {
        return getDescription().getAuthors();
    }

    @Override
    public String getVersion() {
        return getDescription().getVersion();
    }

    @Override
    public boolean isDebug() {
        return getConfig().getBoolean("debug");
    }

    @Override
    public List<Messagable> getDebuggers() {
        return Arrays.asList(new Messagable() {
            @Override
            public void sendMessage(BaseComponent component) {
                getServer().getConsoleSender().spigot().sendMessage(component);
            }

            @Override
            public boolean hasPermission(String permission) {
                return true;
            }

        });
    }

    @Override
    public void callEvent(ESAPIEvent event, Object... data) {
        switch (event) {
        case STORAGE_ENTRY_ADDED:
            getServer().getPluginManager().callEvent(new SpigotStorageEntryAddedEvent((EventEntry) data[0]));
            break;
        default:
            throw new RuntimeException("Unsupported event: " + event.name());
        }
    }

}
