package net.cubekrowd.eventstorageapi.spigot;

import java.util.*;
import java.util.function.*;
import org.bukkit.*;
import org.bukkit.command.*;
import net.md_5.bungee.api.chat.*;
import net.cubekrowd.eventstorageapi.common.command.*;
import lombok.*;

@RequiredArgsConstructor
public class SpigotCommand implements CommandExecutor, ESAPICommand {

    private final EventStoragePluginSpigot plugin;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        executeCommand(plugin, new Messagable() {
            @Override
            public void sendMessage(BaseComponent component) {
                sender.spigot().sendMessage(component);
            }

            @Override
            public boolean hasPermission(String permission) {
                return sender.hasPermission(permission);
            }
        }, args);
        return true;
    }

}
