package net.cubekrowd.eventstorageapi.spigot.event;

import net.cubekrowd.eventstorageapi.api.*;
import net.cubekrowd.eventstorageapi.api.event.*;
import org.bukkit.event.*;
import lombok.*;

@RequiredArgsConstructor
public class SpigotStorageEntryAddedEvent extends Event implements StorageEntryAddedEvent {
    private static final HandlerList handlers = new HandlerList();

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Getter
    private final EventEntry entry;

}
