package net.cubekrowd.eventstorageapi.api;

import java.io.*;
import java.util.*;
import java.util.stream.*;
import org.junit.jupiter.api.*;
import net.cubekrowd.eventstorageapi.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class WorkbenchTest {

    private final EventWorkbench w = new EventWorkbench();

    @BeforeEach
    void testInit() {
    }

    @AfterEach
    void testDown() {
        w.clearCrafting("testplugin");
        w.clearCrafting("someotherplugin");
    }

    @Test
    void testContainsEvent() {
        w.craftEntry("testplugin", "1", "testevent");
        assertTrue(w.containsEntry("testplugin", "1"));
        assertFalse(w.containsEntry("testplugin", "2"));
        assertFalse(w.containsEntry("someotherplugin", "1"));
        assertFalse(w.containsEntry("someotherplugin", "2"));

        w.craftEntry("testplugin", "2", "testevent");
        assertTrue(w.containsEntry("testplugin", "1"));
        assertTrue(w.containsEntry("testplugin", "2"));
        assertFalse(w.containsEntry("someotherplugin", "1"));
        assertFalse(w.containsEntry("someotherplugin", "2"));

        w.craftEntry("someotherplugin", "1", "testevent");
        assertTrue(w.containsEntry("testplugin", "1"));
        assertTrue(w.containsEntry("testplugin", "2"));
        assertTrue(w.containsEntry("someotherplugin", "1"));
        assertFalse(w.containsEntry("someotherplugin", "2"));
    }

    @Test
    void testGetKeys() {
        assertEquals(w.getKeys("testplugin").size(), 0);
        w.craftEntry("testplugin", "1", "testevent");
        assertEquals(w.getKeys("testplugin").size(), 1);
        w.craftEntry("testplugin", "2", "testevent");
        w.craftEntry("testplugin", "3", "testevent");
        assertEquals(w.getKeys("testplugin").size(), 3);
        assertEquals(w.getKeys("testplugin").get(0), "1");
        assertEquals(w.getKeys("testplugin").get(1), "2");
        assertEquals(w.getKeys("testplugin").get(2), "3");
    }

    @Test
    void testRemoveEvent() {
        w.craftEntry("testplugin", "1", "testevent1");
        assertEquals(w.getKeys("testplugin").size(), 1);
        w.craftEntry("testplugin", "2", "testevent2");
        assertEquals(w.getKeys("testplugin").size(), 2);
        w.removeEntry("testplugin", "1");
        assertEquals(w.getKeys("testplugin").size(), 1);
        w.removeEntry("testplugin", "2");
        assertEquals(w.getKeys("testplugin").size(), 0);
    }

    @Test
    void testClearCrafting() {
        w.craftEntry("testplugin", "1", "testevent");
        w.craftEntry("testplugin", "2", "testevent");
        w.craftEntry("someotherplugin", "1", "testevent");
        assertEquals(w.getKeys("testplugin").size(), 2);
        assertEquals(w.getKeys("someotherplugin").size(), 1);
        w.clearCrafting("testplugin");
        assertEquals(w.getKeys("testplugin").size(), 0);
        assertEquals(w.getKeys("someotherplugin").size(), 1);
    }

    @Test
    void testGetEntry() {
        w.craftEntry("testplugin", "1", "testevent1");
        w.craftEntry("testplugin", "2", "testevent2");
        w.craftEntry("someotherplugin", "1", "testevent");
        assertEquals(w.getEntry("testplugin", "1").getType(), "testevent1");
        assertEquals(w.getEntry("testplugin", "2").getType(), "testevent2");
        assertEquals(w.getEntry("someotherplugin", "1").getType(), "testevent");
        assertNull(w.getEntry("someotherplugin", "2"));
    }

}
