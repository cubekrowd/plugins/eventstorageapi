package net.cubekrowd.eventstorageapi.common.storage;

import java.io.*;
import java.util.*;
import java.util.stream.*;
import org.junit.jupiter.api.*;
import net.cubekrowd.eventstorageapi.api.*;
import net.cubekrowd.eventstorageapi.common.*;
import net.cubekrowd.eventstorageapi.common.storage.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class StorageYAMLTest {

    private static final File TEST_DIR = new File("test_data");
    private static final File TEST_FILE = new File(TEST_DIR, "data.yml");

    private EventStorage storage;

    @BeforeEach
    void testInit() {
        TEST_DIR.mkdir();
        storage = new StorageYAML(new EmptyEventStoragePlugin(), TEST_DIR);
        storage.open();
    }

    @AfterEach
    void testDown() {
        storage.close();
        storage = null;
        TEST_FILE.delete();
        TEST_DIR.delete();
    }

    @Test
    void testPlugins() {
        assertEquals(storage.getPlugins().size(), 0);
        storage.addEntry(new EventEntry("testplugin", "testevent", 1));
        assertEquals(storage.getPlugins().size(), 1);
        storage.addEntry(new EventEntry("testplugin", "testevent", 2));
        assertEquals(storage.getPlugins().size(), 1);
        storage.addEntry(new EventEntry("someotherplugin", "testevent", 3));
        assertEquals(storage.getPlugins().size(), 2);
    }

    @Test
    void testEventTypes() {
        assertEquals(storage.getEventTypes("testplugin").size(), 0);
        storage.addEntry(new EventEntry("testplugin", "testevent", 1));
        assertEquals(storage.getEventTypes("testplugin").size(), 1);
        storage.addEntry(new EventEntry("testplugin", "testevent", 2));
        assertEquals(storage.getEventTypes("testplugin").size(), 1);
        storage.addEntry(new EventEntry("testplugin", "testevent2", 3));
        assertEquals(storage.getEventTypes("testplugin").size(), 2);
    }

    @Test
    void testEntryStorage() {
        storage.addEntry(new EventEntry("testplugin", "testevent", 1, Map.of("a", "b", "c", "d")));
        storage.addEntry(new EventEntry("testplugin", "testevent", 2));
        List<EventEntry> entries = storage.getEntryStream("testplugin", "testevent").sorted().collect(Collectors.toList());
        assertEquals(entries.size(), 2);
        assertEquals(entries.get(0).getTime(), 1);
        assertEquals(entries.get(0).getData().size(), 2);
        assertEquals(entries.get(1).getTime(), 2);
        assertEquals(entries.get(1).getData().size(), 0);
    }

}
